# RatesApp

![RatesApp](screenshots/rates-gif.gif)

Android take home Test Revolut

## App description

The app must download and update rates every 1 second using API https://revolut.duckdns.org/latest?base=EUR
List all currencies you get from the endpoint (one per row). Each row has an input where you can enter any amount of money.
When you tap on currency row it should slide to top and its input becomes first responder.
When you’re changing the amount the app must simultaneously update the corresponding value for other currencies.

## Tools used
* [Android Architecture components](https://developer.android.com/topic/libraries/architecture/) for MVVM
* [RxJava2](https://github.com/ReactiveX/RxJava) for asynchronous tasks
* [Dagger 2](https://github.com/google/dagger) for dependency injection
* [Retrofit](https://square.github.io/retrofit/) and [OkHttp](https://github.com/square/okhttp) for network calls
* [Gson](https://github.com/google/gson) for convenient JSON parsing
* [Mockito](https://github.com/mockito/mockito) for mocking dependencies in tests
* [Picasso](https://square.github.io/picasso/) for images loading

## App Architecture
The app architecture is MVVM, which utilize the Android Architecture components.
In the RatesFragment we observe the data from the ViewModel, and the ViewModel gets the data from the Repository.
The repository uses remote data source and in memory cache.

## Known issues
The scrolling lags a little when we are updating the list.










