package com.kukusot.ratesapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kukusot.ratesapp.rates.ui.RatesFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.parent, RatesFragment())
                .commit()
        }
    }
}
