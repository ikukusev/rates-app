package com.kukusot.ratesapp.core.util

import java.text.DecimalFormat

val decimalFormat = DecimalFormat("#0.00")

fun Double.formatted(): String = decimalFormat.format(this)

fun CharSequence?.parseDouble(): Double {
    return try {
        toString().replace(',', '.').takeIf {
            it.isNotBlank()
        }?.toDouble() ?: 0.0
    } catch (exception: NumberFormatException) {
        0.0
    }
}
