package com.kukusot.ratesapp.core.util

import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

fun ViewGroup.inflate(layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun TextView.setTextWithIgnoreListener(text: String, listener: TextWatcher) {
    removeTextChangedListener(listener)
    setText(text)
    addTextChangedListener(listener)
}