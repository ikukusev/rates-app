package com.kukusot.ratesapp.core.di

import dagger.Component
import retrofit2.Retrofit

@ApplicationScope
@Component(modules = [HttpModule::class])
interface AppComponent {

    fun retrofit(): Retrofit
}