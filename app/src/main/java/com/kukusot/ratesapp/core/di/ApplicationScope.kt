package com.kukusot.ratesapp.core.di

import javax.inject.Scope

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class ApplicationScope