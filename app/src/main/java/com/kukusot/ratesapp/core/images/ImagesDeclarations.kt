package com.kukusot.ratesapp.core.images

import android.widget.ImageView
import com.kukusot.ratesapp.BuildConfig
import com.squareup.picasso.Picasso
import jp.wasabeef.picasso.transformations.CropCircleTransformation

fun ImageView.displayFlag(countryCode: String) {
    Picasso.get()
        .load(BuildConfig.FLAGS_BASE_URL + countryCode + ".png")
        .transform(
            CropCircleTransformation()
        ).into(this)
}