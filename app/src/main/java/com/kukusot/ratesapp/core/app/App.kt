package com.kukusot.ratesapp.core.app

import android.app.Application
import com.kukusot.ratesapp.core.di.AppComponent
import com.kukusot.ratesapp.core.di.DaggerAppComponent

class App : Application() {

    val appComponent: AppComponent = DaggerAppComponent.create()
}