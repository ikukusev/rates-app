package com.kukusot.ratesapp.rates.domain

import com.kukusot.ratesapp.R
import java.lang.IllegalArgumentException

enum class RateResource(val nameRes: Int, val countryCode: String) {
    EUR(R.string.eur_name, "eu"),
    AUD(R.string.aud_name, "au"),
    BGN(R.string.bgn_name, "bg"),
    BRL(R.string.brl_name, "br"),
    CAD(R.string.cad_name, "ca"),
    CHF(R.string.chf_name, "ch"),
    CNY(R.string.cny_name, "cn"),
    CZK(R.string.czk_name, "cz"),
    DKK(R.string.dkk_name, "dk"),
    GBP(R.string.gbp_name, "gb"),
    HKD(R.string.hkd_name, "hk"),
    HRK(R.string.hrk_name, "hr"),
    HUF(R.string.huf_name, "hu"),
    IDR(R.string.idr_name, "id"),
    ILS(R.string.ils_name, "il"),
    INR(R.string.inr_name, "in"),
    ISK(R.string.isk_name, "is"),
    JPY(R.string.jpy_name, "jp"),
    KRW(R.string.krw_name, "kr"),
    MXN(R.string.mxn_name, "mx"),
    MYR(R.string.myr_name, "my"),
    NOK(R.string.nok_name, "no"),
    NZD(R.string.nzd_name, "nz"),
    PHP(R.string.php_name, "ph"),
    PLN(R.string.pln_name, "pl"),
    RUB(R.string.rub_name, "ru"),
    SEK(R.string.sek_name, "se"),
    SGD(R.string.sgd_name, "sg"),
    THB(R.string.thb_name, "th"),
    TRY(R.string.try_name, "tr"),
    USD(R.string.usd_name, "us"),
    ZAR(R.string.zar_name, "za"),
    RON(R.string.ron_name, "ro"),
    NAN(R.string.no_name, "nan")
}

fun resourceForSymbol(symbol: String): RateResource {
    return try {
        RateResource.valueOf(symbol)
    } catch (exception: IllegalArgumentException) {
        RateResource.NAN
    }
}