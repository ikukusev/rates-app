package com.kukusot.ratesapp.rates.ui

import com.kukusot.ratesapp.rates.domain.RateListItem

sealed class RatesUiData {
    data class Success(val data: List<RateListItem>): RatesUiData()
    data class Error(val error: Throwable): RatesUiData()
}