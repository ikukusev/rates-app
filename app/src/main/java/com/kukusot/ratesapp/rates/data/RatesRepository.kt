package com.kukusot.ratesapp.rates.data

import androidx.annotation.VisibleForTesting
import com.kukusot.ratesapp.rates.remote.RatesRemoteDataSource
import io.reactivex.Single
import javax.inject.Inject

class RatesRepository @Inject constructor(private val remoteDataSource: RatesRemoteDataSource) {

    private var cachedRates: Rates = Rates.empty()

    fun getRates(base: String, forceReload: Boolean): Single<Rates> {
        return if (!forceReload && cachedRates.base == base) {
            Single.just(cachedRates)
        } else {
            remoteDataSource.getRates(base).doOnSuccess { rates ->
                cachedRates = rates
            }
        }
    }

    @VisibleForTesting
    fun setCachedRates(rates: Rates) {
        cachedRates = rates
    }
}