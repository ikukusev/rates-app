package com.kukusot.ratesapp.rates.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.kukusot.ratesapp.R
import com.kukusot.ratesapp.core.util.inflate
import com.kukusot.ratesapp.rates.domain.RateListItem

class RatesAdapter(private val listener: RatesAdapterListener) :
    ListAdapter<RateListItem, RateViewHolder>(DIFF_CALLBACK) {

    interface RatesAdapterListener {

        fun onRateClicked(currencySymbol: String, amount: Double)

        fun onAmountChanged(amount: Double)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder =
        RateViewHolder(
            parent.inflate(
                R.layout.item_rate,
                false
            )
        ).apply {
            adapterListener = listener
        }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<RateListItem>() {

            override fun areItemsTheSame(
                oldItem: RateListItem,
                newItem: RateListItem
            ): Boolean = oldItem.symbol == newItem.symbol

            override fun areContentsTheSame(
                oldItem: RateListItem,
                newItem: RateListItem
            ): Boolean = oldItem.editEnabled && newItem.editEnabled || oldItem == newItem
        }
    }
}