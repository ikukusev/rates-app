package com.kukusot.ratesapp.rates.data

data class Rate(val symbol: String, val rate: Double)