package com.kukusot.ratesapp.rates.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.kukusot.ratesapp.rates.domain.FetchRatesUseCase

class RatesViewModelFactory constructor(
    private val fetchRatesUseCase: FetchRatesUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RatesViewModel(fetchRatesUseCase) as T
    }
}