package com.kukusot.ratesapp.rates.remote

import com.kukusot.ratesapp.rates.data.Rate
import com.kukusot.ratesapp.rates.data.Rates
import io.reactivex.Single
import javax.inject.Inject

class RatesRemoteDataSource @Inject constructor(private val api: RatesApi) {

    fun getRates(base: String): Single<Rates> = api.getRates(base).map { response ->
        val ratesList = response.ratesMap.map { entry ->
            Rate(entry.key, entry.value)
        }
        Rates(base, ratesList)
    }
}