package com.kukusot.ratesapp.rates.di

import androidx.lifecycle.ViewModelProvider
import com.kukusot.ratesapp.rates.domain.FetchRatesUseCase
import com.kukusot.ratesapp.rates.remote.RatesApi
import com.kukusot.ratesapp.rates.ui.RatesViewModelFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object RatesModule {

    @Provides
    @RatesScope
    fun provideRatesApi(retrofit: Retrofit): RatesApi =
        retrofit.create(RatesApi::class.java)

    @Provides
    @RatesScope
    fun provideCurrencyViewModelFactory(
        fetchRatesUseCase: FetchRatesUseCase
    ): ViewModelProvider.Factory =
        RatesViewModelFactory(fetchRatesUseCase)

}