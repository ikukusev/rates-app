package com.kukusot.ratesapp.rates.domain

import com.kukusot.ratesapp.rates.data.Rates
import com.kukusot.ratesapp.rates.data.RatesRepository
import io.reactivex.Single
import javax.inject.Inject

class FetchRatesUseCase @Inject constructor(private val repository: RatesRepository) {

    operator fun invoke(
        base: String,
        baseAmount: Double,
        forceReload: Boolean
    ): Single<List<RateListItem>> {
        return repository.getRates(base, forceReload).map { rates ->
            createListItems(baseAmount, rates)
        }
    }

    private fun createListItems(
        baseAmount: Double,
        rates: Rates
    ): ArrayList<RateListItem> {
        val rateResource = resourceForSymbol(rates.base)
        val items = arrayListOf(
            RateListItem(
                symbol = rates.base,
                amount = baseAmount,
                nameRes = rateResource.nameRes,
                countryCode = rateResource.countryCode,
                editEnabled = true
            )
        )
        rates.rateList.forEach { rateEntry ->
            val resource = resourceForSymbol(rateEntry.symbol)
            items.add(
                RateListItem(
                    symbol = rateEntry.symbol,
                    amount = baseAmount * rateEntry.rate,
                    nameRes = resource.nameRes,
                    countryCode = resource.countryCode,
                    editEnabled = false
                )
            )
        }
        return items
    }
}