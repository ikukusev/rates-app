package com.kukusot.ratesapp.rates.data

data class Rates(val base: String, val rateList: List<Rate>) {

    companion object {
        fun empty() = Rates("", emptyList())
    }
}
