package com.kukusot.ratesapp.rates.ui

import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.kukusot.ratesapp.core.images.displayFlag
import com.kukusot.ratesapp.core.util.formatted
import com.kukusot.ratesapp.core.util.parseDouble
import com.kukusot.ratesapp.core.util.setTextWithIgnoreListener
import com.kukusot.ratesapp.rates.domain.RateListItem
import kotlinx.android.synthetic.main.item_rate.view.*

class RateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val symbolText = itemView.item_symbol
    private val nameText = itemView.item_name
    private val flagImage = itemView.item_flag
    private val amountText = itemView.item_amount

    lateinit var adapterListener: RatesAdapter.RatesAdapterListener

    private val amountTextWatcher = amountText.doAfterTextChanged {
        adapterListener.onAmountChanged(it.parseDouble())
    }

    fun bind(rateItem: RateListItem) {
        symbolText.text = rateItem.symbol
        amountText.setTextWithIgnoreListener(rateItem.amount.formatted(), amountTextWatcher)
        flagImage.displayFlag(rateItem.countryCode)
        nameText.setText(rateItem.nameRes)

        itemView.setOnClickListener {
            if (rateItem.editEnabled) {
                return@setOnClickListener
            }

            amountText.requestFocus()
            adapterListener.onRateClicked(
                rateItem.symbol,
                amountText.text.parseDouble()
            )
        }

        amountText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                adapterListener.onRateClicked(
                    rateItem.symbol,
                    amountText.text.parseDouble()
                )
            }
        }
    }
}