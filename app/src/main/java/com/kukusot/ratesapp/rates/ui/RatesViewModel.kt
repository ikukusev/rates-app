package com.kukusot.ratesapp.rates.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kukusot.ratesapp.BuildConfig
import com.kukusot.ratesapp.rates.domain.FetchRatesUseCase
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class RatesViewModel(
    private val fetchRatesUseCase: FetchRatesUseCase
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private var fetchCurrenciesDisposable: Disposable? = null

    private var baseSymbol: String = BuildConfig.DEFAULT_RATE
    private var baseAmount: Double = BuildConfig.DEFAULT_RATE_AMOUNT

    private val _currenciesData = MutableLiveData<RatesUiData>()
    val currenciesData: LiveData<RatesUiData> = _currenciesData

    fun start() {
        fetchRatesPeriodically()
    }

    private fun fetchRatesPeriodically() {
        fetchCurrenciesDisposable?.dispose()
        fetchCurrenciesDisposable = fetchRatesUseCase(baseSymbol, baseAmount, true)
            .repeatWhen { completed ->
                completed.delay(REPEAT_INTERVAL, TimeUnit.MILLISECONDS)
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _currenciesData.value = RatesUiData.Success(it)
            }, {
                _currenciesData.value = RatesUiData.Error(it)
            })
    }

    fun changeBaseRate(symbol: String, amount: Double) {
        baseSymbol = symbol
        baseAmount = amount
        fetchRatesPeriodically()
    }

    fun changeBaseRateAmount(amount: Double) {
        baseAmount = amount
        fetchCurrenciesDisposable?.dispose()
        fetchRatesUseCase(baseSymbol, baseAmount, false)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _currenciesData.value = RatesUiData.Success(it)
                fetchRatesPeriodically()
            },
                {
                    fetchRatesPeriodically()
                }).addTo(compositeDisposable)
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    companion object {
        private const val REPEAT_INTERVAL = 1000L
    }
}