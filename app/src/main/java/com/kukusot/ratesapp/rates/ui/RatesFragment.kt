package com.kukusot.ratesapp.rates.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.kukusot.ratesapp.R
import com.kukusot.ratesapp.core.app.App
import com.kukusot.ratesapp.core.util.inflate
import com.kukusot.ratesapp.rates.di.DaggerRatesComponent
import kotlinx.android.synthetic.main.fragment_currencies.*
import javax.inject.Inject


class RatesFragment : Fragment(), RatesAdapter.RatesAdapterListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: RatesViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(RatesViewModel::class.java)
    }
    private lateinit var ratesAdapter: RatesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.start()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerRatesComponent
            .builder()
            .appComponent((context.applicationContext as App).appComponent)
            .build()
            .inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return container?.inflate(R.layout.fragment_currencies, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currencyList.apply {
            setHasFixedSize(true)
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            layoutManager = LinearLayoutManager(this.context)
            ratesAdapter = RatesAdapter(this@RatesFragment)
            adapter = ratesAdapter
        }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.currenciesData.observe(viewLifecycleOwner, Observer { uiData ->
            when (uiData) {
                is RatesUiData.Success -> ratesAdapter.submitList(uiData.data)
                is RatesUiData.Error -> context?.let {
                    Toast.makeText(it, R.string.error_fetching_rates, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onRateClicked(currencySymbol: String, amount: Double) {
        viewModel.changeBaseRate(currencySymbol, amount)
    }

    override fun onAmountChanged(amount: Double) {
        viewModel.changeBaseRateAmount(amount)
    }
}