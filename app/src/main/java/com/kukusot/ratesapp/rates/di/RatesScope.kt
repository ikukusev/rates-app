package com.kukusot.ratesapp.rates.di

import javax.inject.Scope

@Retention(value = AnnotationRetention.RUNTIME)
@Scope
annotation class RatesScope