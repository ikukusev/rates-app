package com.kukusot.ratesapp.rates.di

import com.kukusot.ratesapp.rates.ui.RatesFragment
import com.kukusot.ratesapp.core.di.AppComponent
import dagger.Component

@RatesScope
@Component(dependencies = [AppComponent::class], modules = [RatesModule::class])
interface RatesComponent {

    fun inject(ratesFragment: RatesFragment)
}