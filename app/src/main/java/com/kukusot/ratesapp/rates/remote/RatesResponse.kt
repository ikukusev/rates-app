package com.kukusot.ratesapp.rates.remote

import com.google.gson.annotations.SerializedName

data class RatesResponse(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: String,
    @SerializedName("rates") val ratesMap: Map<String, Double>
)