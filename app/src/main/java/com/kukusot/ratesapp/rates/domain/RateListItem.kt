package com.kukusot.ratesapp.rates.domain

data class RateListItem(
    val symbol: String,
    val amount: Double,
    val nameRes: Int,
    val countryCode: String,
    val editEnabled: Boolean
)