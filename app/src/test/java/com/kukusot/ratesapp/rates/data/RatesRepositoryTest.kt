package com.kukusot.ratesapp.rates.data

import com.kukusot.ratesapp.rates.remote.RatesRemoteDataSource
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

class RatesRepositoryTest {

    val remoteDataSource: RatesRemoteDataSource = mock()
    val ratesRepository = RatesRepository(remoteDataSource)

    @Test
    fun `getRates should return cachedRates when cached rates base is equal and forceReload is false`() {
        val cachedRates = Rates("base", listOf())
        ratesRepository.setCachedRates(cachedRates)

        ratesRepository.getRates("base", false).test().assertValue(cachedRates)
    }

    @Test
    fun `getRates should return rates from remoteDataSource when cached rates base is equal and forceReload is true`() {
        val remoteRates = Rates("base", listOf())
        val cachedRates = Rates("base", listOf(Rate("s", 1.0)))
        whenever(remoteDataSource.getRates("base")).thenReturn(Single.just(remoteRates))
        ratesRepository.setCachedRates(cachedRates)

        ratesRepository.getRates("base", true).test().assertValue(remoteRates)
    }

    @Test
    fun `getRates should return rates from remoteDataSource when cached rates base is not equal and forceReload is false`() {
        val remoteRates = Rates("base", listOf())
        val cachedRates = Rates("notBase", listOf(Rate("s", 1.0)))
        whenever(remoteDataSource.getRates("base")).thenReturn(Single.just(remoteRates))
        ratesRepository.setCachedRates(cachedRates)

        ratesRepository.getRates("base", true).test().assertValue(remoteRates)
    }
}