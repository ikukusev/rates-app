package com.kukusot.ratesapp.rates.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kukusot.ratesapp.BuildConfig
import com.kukusot.ratesapp.rates.domain.FetchRatesUseCase
import com.kukusot.ratesapp.rates.domain.RateListItem
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.plugins.RxAndroidPlugins.*
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RatesViewModelTest {

    @Rule
    @JvmField
    val instantTasRule = InstantTaskExecutorRule()

    val useCase: FetchRatesUseCase = mock()
    val viewModel = RatesViewModel(useCase)

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        setMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun `start should post rates for base rate when usecase is executed successfully`() {
        val rateItems: List<RateListItem> = mock()
        whenever(
            useCase.invoke(
                BuildConfig.DEFAULT_RATE,
                BuildConfig.DEFAULT_RATE_AMOUNT,
                true
            )
        ).thenReturn(
            Single.just(rateItems)
        )

        viewModel.start()

        assertEquals(RatesUiData.Success(rateItems), viewModel.currenciesData.value)
    }

    @Test
    fun `start should post error when usecase execution throws an error`() {
        val error = Exception("ex")
        whenever(
            useCase.invoke(
                BuildConfig.DEFAULT_RATE,
                BuildConfig.DEFAULT_RATE_AMOUNT,
                true
            )
        ).thenReturn(
            Single.error(error)
        )

        viewModel.start()

        assertEquals(RatesUiData.Error(error), viewModel.currenciesData.value)
    }

    @Test
    fun `changeBaseRate should change the base rate and post new rates when usecase is executed successfully`() {
        val newBase = "newBase"
        val newAmount = 2.0
        val rateItems: List<RateListItem> = mock()
        whenever(
            useCase.invoke(
                newBase,
                newAmount,
                true
            )
        ).thenReturn(
            Single.just(rateItems)
        )

        viewModel.changeBaseRate(newBase, newAmount)

        assertEquals(RatesUiData.Success(rateItems), viewModel.currenciesData.value)
    }

    @Test
    fun `changeBaseRate should change the base rate and post error when usecase throws an error`() {
        val newBase = "newBase"
        val newAmount = 2.0
        val error = Exception("ex")
        whenever(useCase.invoke(newBase, newAmount, true)).thenReturn(
            Single.error(error)
        )

        viewModel.changeBaseRate(newBase, newAmount)

        assertEquals(RatesUiData.Error(error), viewModel.currenciesData.value)
    }

    @Test
    fun `changeBaseAmount should change the amount and fetch rates without force reload`() {
        val rateItems: List<RateListItem> = mock()
        val newAmount = 30.0
        whenever(useCase.invoke(BuildConfig.DEFAULT_RATE, newAmount, false)).thenReturn(
            Single.just(rateItems)
        )

        whenever(useCase.invoke(BuildConfig.DEFAULT_RATE, newAmount, true)).thenReturn(
            Single.just(rateItems)
        )

        viewModel.changeBaseRateAmount(newAmount)

        assertEquals(RatesUiData.Success(rateItems), viewModel.currenciesData.value)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }


}