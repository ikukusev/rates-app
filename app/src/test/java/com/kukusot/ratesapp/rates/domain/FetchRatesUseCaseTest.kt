package com.kukusot.ratesapp.rates.domain

import com.kukusot.ratesapp.R
import com.kukusot.ratesapp.rates.data.Rate
import com.kukusot.ratesapp.rates.data.Rates
import com.kukusot.ratesapp.rates.data.RatesRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

class FetchRatesUseCaseTest {

    val ratesRepository: RatesRepository = mock()
    val useCase = FetchRatesUseCase(ratesRepository)

    @Test
    fun `index 0 of usecase result should be base editable currency`() {
        whenever(ratesRepository.getRates("EUR", false)).thenReturn(
            Single.just(
                Rates(
                    "EUR",
                    listOf()
                )
            )
        )

        useCase("EUR", 1.0, false).test().assertValue {
            RateListItem("EUR", 1.0, R.string.eur_name, "eu", true) == it[0]
        }
    }

    @Test
    fun `not base items should be correctly mapped with correct values`() {
        whenever(ratesRepository.getRates("EUR", false)).thenReturn(
            Single.just(
                Rates(
                    "EUR",
                    listOf(Rate("USD", 1.43))
                )
            )
        )

        useCase("EUR", 1.0, false).test().assertValue {
            RateListItem("USD", 1.43, R.string.usd_name, "us", false) == it[1]
        }
    }
}